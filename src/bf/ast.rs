/**
 * 構文解析
 */

use super::opcode::OpCode;

pub struct AST {
    pub code: OpCode,
    pub block: Vec<AST>,
}