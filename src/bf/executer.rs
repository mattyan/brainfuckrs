/**
 * brainfuckコードを実行
 */

use super::parser::parse2;
use super::opcode::OpCode;
use super::ast::AST;
use std::io::BufRead;

const BUF_MAX: usize = 4000; // バッファは4KBまで

/**
 * コードをコンパイルして実行
 */
pub fn execute2(code :&mut dyn BufRead) -> Result<(), ()> {
    let mut buffer = [0 as u8; BUF_MAX];
    let ast = parse2(code).unwrap();
    exec(&ast, &mut buffer, 0);
    Ok(())
}

/**
 * ASTを実行
 */
fn exec(code_list: &Vec<AST>, buf: &mut [u8; BUF_MAX], mut index: usize) {
    let mut output_buffer: Vec<u8> = Vec::new();
    for code in code_list {
        match code.code {
            OpCode::PointerIncrement => {
                index = if index < BUF_MAX { index + 1 } else { index };
            },
            OpCode::PointerDecrement => {
                index = if index > 0 { index - 1 } else { index };
            },
            OpCode::ValueIncrement => {
                buf[index] = if buf[index] < 0xff { buf[index] + 1 } else { buf[index] };
            },
            OpCode::ValueDecrement => {
                buf[index] = if buf[index] > 0 { buf[index] - 1 } else { buf[index] };
            },
            OpCode::Output => {
                output_buffer.push(buf[index]);
                let res = String::from_utf8(output_buffer.clone());
                match res {
                    Ok(s) => {
                        print!("{}", s);
                        output_buffer.clear();
                    }
                    Err(_) => {}
                }
            },
            OpCode::Loop => {
                while buf[index] > 0 {
                    exec(&code.block, buf, index);
                }
            }
        }
    }
}