/**
 * 命令コード
 */
pub enum OpCode {
    PointerIncrement = 0, // >
    PointerDecrement, // <
    ValueIncrement, // +
    ValueDecrement, // -
    Output, // .
    Loop, // []
}