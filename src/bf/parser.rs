/**
 * 字句解析
 */
use super::ast::AST;
use super::opcode::OpCode;
use std::io::{ BufRead };

pub fn parse2(code_stream: &mut dyn BufRead) -> Result<Vec<AST>, String> {
    let mut code_list: Vec<AST> = Vec::new();
    let mut c = [0; 1];
    loop{
        if code_stream.read(&mut c).unwrap() == 0 { break; }
        let code = String::from_utf8(c.to_vec()).unwrap();
        match code.as_str() {
            ">" => {
                let ast = AST{
                    code: OpCode::PointerIncrement,
                    block: vec![],
                };
                code_list.push(ast);
            },
            "<" => {
                let ast = AST {
                    code: OpCode::PointerDecrement,
                    block: vec![],
                };
                code_list.push(ast);
            },
            "+" => {
                let ast = AST {
                    code: OpCode::ValueIncrement,
                    block: vec![],
                };
                code_list.push(ast);
            },
            "-" => {
                let ast = AST {
                    code: OpCode::ValueDecrement,
                    block: vec![],
                };
                code_list.push(ast);
            },
            "." => {
                let ast = AST {
                    code: OpCode::Output,
                    block: vec![],
                };
                code_list.push(ast);
            },
            "[" => {
                let block = parse2(code_stream)?;
                let ast = AST {
                    code: OpCode::Loop,
                    block: block,
                };
                code_list.push(ast);
            },
            "]" => {
                return Ok(code_list);
            }
            "\r" => {}
            "\n" => { break; }
            _ => { return Err(format!("不正なシンボル {}", code)); }
        }
    }

    Ok(code_list)
}
