extern crate brainfuckrs;

use brainfuckrs::bf::execute2;
use std::env;
use std::io::{stdin, stdout, BufReader};
use std::fs::File;
use std::io::prelude::*;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() == 2 {
        // ファイルから読み込み
        let mut reader = BufReader::new(File::open(&args[1]).unwrap());
        execute2(&mut reader).unwrap();
    } else {
        // stdinから読み込み
        print!("code: ");
        stdout().flush().ok();
        execute2(&mut stdin().lock()).unwrap();
    }
}
